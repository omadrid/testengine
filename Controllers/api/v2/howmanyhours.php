<?php
/**
 * Minds How Many Hours API
 *
 * @version 1.1
 * @author Olivia Madrid
 */

namespace Minds\Controllers\api\v2;

use Minds\Api\Factory;
use Minds\Core;
use Minds\Interfaces;

class howmanyhours implements Interfaces\Api
{
    public function get($pages)
    {
        $user = Core\Session::getLoggedInUser();
        $response = [
            'seconds' => (int)$user->getTimeCreated(),
        ];

        return Factory::response($response);
    }

    public function post($pages)
    {
        return Factory::response(array());
    }

    public function put($pages)
    {
        return Factory::response(array());
    }

    public function delete($pages)
    {
        return Factory::response(array());
    }

}
